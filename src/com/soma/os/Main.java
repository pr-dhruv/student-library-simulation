package com.soma.os;

import com.soma.os.common.Constants;
import com.soma.os.entity.Book;
import com.soma.os.entity.Student;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = null;
        Student[] students = null;
        Book[] books = null;

        try {
            executorService = Executors.newFixedThreadPool(Constants.NUMBER_OF_STUDENT);
            students = new Student[Constants.NUMBER_OF_STUDENT];
            books = new Book[Constants.NUMBER_OF_BOOKS];

            for (int i = 0; i < Constants.NUMBER_OF_BOOKS; i++)
                books[i] = new Book(i);

            for (int i = 0; i < Constants.NUMBER_OF_STUDENT; i++) {
                students[i] = new Student(i, books);
                executorService.execute(students[i]);
            }
            Thread.sleep(Constants.SIMULATION_TIME);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown();
            while(!executorService.isTerminated()) {
                Thread.sleep(1000);
            }
            for (Student s : students) {
                System.out.println(s + " read " + s.getReadCount());
            }
        }

    }
}
