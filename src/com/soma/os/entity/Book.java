package com.soma.os.entity;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public class Book {
    private int id;
    private Lock lock;
    private Random random;

    public Book(int id) {
        this.id = id;
        this.lock = new ReentrantLock();
        this.random = new Random();
    }

    public boolean read(Student student) throws InterruptedException {
        if (lock.tryLock(100, TimeUnit.MILLISECONDS)) {
            System.out.println(student + " starts reading " + this);
            Thread.sleep(random.nextInt(2000) + 1);
            lock.unlock();
            System.out.println(student + " has finished " + this);
            return true;
        } else {
            System.out.println(student + " didn't get " + this);
            return false;
        }
    }

    @Override
    public String toString() {
        return "Book " + this.id;
    }

}
