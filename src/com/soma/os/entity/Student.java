package com.soma.os.entity;

import com.soma.os.common.Constants;

import java.util.Arrays;
import java.util.Random;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public class Student implements Runnable {
    private int id;
    private Book[] books;
    private Random random;
    private int[] readCount;

    public Student(int id, Book[] books) {
        this.id = id;
        this.books = books;
        random = new Random();
        this.readCount = new int[books.length];
    }

    public String getReadCount() {
        return Arrays.toString(readCount);
    }

    @Override
    public void run() {
        long stopTime = System.currentTimeMillis() + Constants.SIMULATION_TIME;
        while (System.currentTimeMillis() < stopTime) {
            int bookId = random.nextInt(Constants.NUMBER_OF_BOOKS);

            try {
                if(books[bookId].read(this))
                    readCount[bookId]++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return "Student " + this.id;
    }

}
