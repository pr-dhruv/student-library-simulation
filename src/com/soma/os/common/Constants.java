package com.soma.os.common;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public interface Constants {
    int NUMBER_OF_STUDENT = 5;
    int NUMBER_OF_BOOKS = 7;
    int SIMULATION_TIME = 20 * 1000;
}
